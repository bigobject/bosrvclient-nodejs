# BigObject Service client

This is an RPC client for BigObject Service powered by Apache Thrift.

- [What is BigObject](http://docs.macrodatalab.com/)
- [Get BigObject on HEROKU](https://addons.heroku.com/bigobject)

# Getting this package

The recommended way to get latest build from this repo is through
[npm](https://www.npmjs.com/)

```
$ npm install bosrvclient
```

# Basic usage instructions

bosrvclient utilizes HTTP/HTTPS transport and Apache Thrift JSON protocol for
delivering and recving data.

To create a connection helper, start by including **bosrvclient**

```javascript
var bosrv = require('bosrvclient');
var conn = new bosrv.Connection(<uri_to_bigobject>, <timeout>);

var token = conn.token;
var client = conn.client;
```

A typical query structure can be layout into the following:

- Specify and send your query via execute method
- For methods returning a handle (sha256 string), use cursor helpers to get
  data back
- Data retrieved are encoded as JSON string.

```javascript
client.execute(token, <assoc_stmt>, '', '', function (err, res) {
    if (err) throw err;
    if (res)
    {
        var rngspec = new bosrv.ttypes.RangeSpec(<start>, <page size>);
        client.cursor_fetch(token, res, rngspec, function (err, data) {
            var data = JSON.parse(data);
            var eol = data[0]; // if eol == -1, then no more data to read
            var rows = data[1];
        });
    }
});
```

## Race conditions with cursor access

While all operations can be interleaved, *cursor* keep track of access progress
through internal data update.  To avoid race condition, specify *start* to in
**RangeSpec** to take hold of indexing at each *cursor\_fetch*.

# Handling resource references

We strongly urge you to close the returned resource.  While garbage collection
is done routinely on the BigObject service, it intended for unexpected
termination from client application, hence the garbage collection cycle is
kept at a minimal pace.

While it is valid to cache the returned resource handle, make sure your
application handles exceptions accordingly.

# Exceptions produced by API during runtime

## AuthError

produced when providing an invalid authentication token

## ServiceError

general mishaps or signs of really bad server state
