// Import BigObjectService module
var exception_ttypes = require('./exception_types'),
    bosrv_ttypes = require('./BigObjectService_types'),
    bosrv_client = require('./BigObjectService').Client;

// Import supporting modules
var thrift = require('thrift'),
    url = require('url');


var Connection = function (addr_info, timeout)
{
    // parse provided URL from user
    addr_info = addr_info || '';
    var uri = url.parse(addr_info);
    var host = uri.hostname;
    var token = ( uri.auth || ':' ).split(':')[1];
    var path = uri.path;

    var opts = {
        transport: thrift.TBufferedTransport,
        protocol: thrift.TJSONProtocol,
        path: path,
        headers: { "Connection": "close" },
        https: uri.protocol === 'bos:',
        nodeOptions: {
            rejectUnauthorized: false,
        }
    };

    var port = uri.port || (opts.https ? 443 : 80);

    this.conn = thrift.createHttpConnection(host, port, opts);
    this.client = thrift.createHttpClient(bosrv_client, this.conn);
    this.token = token;
}

// Allowed export from bosrvclient module
module.exports = {
    exc_ttypes: exception_ttypes,
    ttypes: bosrv_ttypes,
    Connection: Connection
};
