var addr_info = process.env.BIGOBJECT_URL;

var bosrv = require('../');

// Instantiate the client handle
var conn = new bosrv.Connection(addr_info);
var token = conn.token;
var client = conn.client;

var tables = require('./tables');
var driver = tables.driver;

// setup example environment
tables.setup_environ(addr_info);
// populate data for our target query table
tables.populate_steps(addr_info, true, 1);

// All test tables and data populated, begin Cross-Link example
// First build that query target
driver.on('init', function () {
    var build_assoc_stmt = "build association q3(shoes.brand) by users.id from steps";
    var begin = Date.now();
    client.execute(token, build_assoc_stmt, '', '', function (err) {
        if (err)
        {
            console.log(err.why);
            console.log(err.misc);
            throw err;
        }
        var end = Date.now();
        console.log('--------------------------');
        console.log(build_assoc_stmt);
        console.log('Operation took time: ' + (end - begin) / 1000);
        console.log('--------------------------\n');
        driver.emit('query');
    });
});

// Then issue query to the built target
driver.on('query', function () {
    var query_stmt = "get 10 freq('Sperry') from q3";
    var begin = Date.now();
    client.execute(token, query_stmt, '', '', function (err, res) {
        if (err)
        {
            console.log(err.why);
            console.log(err.misc);
            throw err;
        }
        var end = Date.now();
        console.log('--------------------------');
        console.log(query_stmt);
        console.log('Operation took time: ' + (end - begin) / 1000);
        console.log('--------------------------\n');
        driver.emit('data', res);
    });
});

var result_table = [];
var rngspec = new bosrv.ttypes.RangeSpec({ page: 100 });

// Fetch data from cursor
driver.on('data', function (res) {
    var begin = Date.now();
    client.cursor_fetch(token, res, rngspec, function (err, data) {
        if (err)
        {
            console.log(err.why);
            console.log(err.misc);
            throw err;
        }
        data = JSON.parse(data);
        Array.prototype.push.apply(result_table, data[1]);
        if (data[0] == -1)
        {
            var end = Date.now();
            console.log('--------------------------');
            console.log('Time spent to retrieve from cursor: ' + (end - begin) / 1000);
            result_table.forEach(function (row) {
                console.log('(' + row.join(', ') + ')');
            });
            console.log('--------------------------');
            driver.emit('end', res);
        }
        else
        {
            driver.emit("data", res);
        }
    });
});

// Query result data fetched, close cursor handle
driver.on('end', function (res) {
    client.cursor_close(token, res, function () {});
});
