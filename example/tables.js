var EventEmitter = require('events').EventEmitter;

var bosrv = require('../');

var users = [
    [1, 'M', 'Taiwan'],
    [2, 'M', 'US'],
    [3, 'F', 'Sleepy Hollow'],
    [4, 'F', 'Taiwan'],
    [5, 'M', 'Japan'],
    [6, 'F', 'United Kindom'],
    [7, 'F', 'Scott'],
    [8, 'F', 'Happy Tree Valley']
];

var shoes = [
    [1, 'Toms'],
    [2, 'Sperry'],
    [3, 'Kate'],
    [4, 'Sofi'],
    [5, 'McClain'],
    [6, 'Cherry'],
    [7, 'Miu Miu'],
    [8, 'Salvatore Ferrogamo'],
];

var completed = [];
var mainloop = new EventEmitter();

mainloop.on('ready', function (which) {
    completed.push(which);
    if (completed.indexOf('users') != -1 &&
        completed.indexOf('shoes') != -1 &&
        completed.indexOf('steps') != -1)
    {
        mainloop.emit('init');
    }
});

module.exports = {
    driver: mainloop,

    setup_environ: function (addr_info)
    {
        var handle = new bosrv.Connection(addr_info);

        // connect to remote RPC target
        var token = handle.token;
        var client = handle.client;

        client.execute(
            token,
            'CREATE TABLE users (id INT, gender STRING, country STRING, KEY (id))',
            '', '',
            function (err) {
                if (err)
                {
                    console.log(err.why);
                    console.log(err.misc);
                    throw err;
                }
                var insert_stmt = 'INSERT INTO users VALUES ' +
                    users.map(function (u, i) {
                        return '(' + u[0] + ',' + u[1] + ',\'' + u[2] + '\')';
                    })
                    .join(' ');
                client.execute(token, insert_stmt, '', '', function (err) {
                    if (err)
                    {
                        console.log(err.why);
                        console.log(err.misc);
                        throw err;
                    }
                    mainloop.emit('ready', 'users');
                });
            }
        );

        client.execute(
            token,
            'CREATE TABLE shoes (id INT, brand STRING, KEY(id))',
            '', '',
            function (err) {
                if (err)
                {
                    console.log(err.why);
                    console.log(err.misc);
                    throw err;
                }
                var insert_stmt = 'INSERT INTO shoes VALUES ' +
                    shoes.map(function (s, i) {
                        return '(' + s[0] + ',\'' + s[1] + '\')';
                    })
                    .join(' ');
                client.execute(token, insert_stmt, '', '', function (err) {
                    if (err)
                    {
                        console.log(err.why);
                        console.log(err.misc);
                        throw err;
                    }
                    mainloop.emit('ready', 'shoes');
                });
            }
        );
    },

    populate_steps: function (addr_info, report, count)
    {
        var handle = new bosrv.Connection(addr_info);

        // connect to remote RPC target
        var token = handle.token;
        var client = handle.client;

        var gen_step_tuple = function ()
        {
            return [
                Math.floor(Math.random() * users.length) + 1,
                Math.floor(Math.random() * shoes.length) + 1,
                Math.floor(Math.random() * 10) + 1,
            ];
        };

        var iterations = count;

        var begin = Date.now();
        mainloop.on('populate', function () {
            var insert_stmt = 'INSERT INTO steps VALUES ' + function ()
            {
                var arr = [];
                for (var it = 0; it < 10000; it++)
                {
                    arr.push('(' + gen_step_tuple().join(',') + ')');
                }
                return arr;
            }()
            .join(' ');

            client.execute(token, insert_stmt, '', '', function (err) {
                if (err)
                {
                    console.log(err.why);
                    console.log(err.misc);
                    throw err;
                }
                if (iterations == 0)
                {
                    var end = Date.now();
                    console.log('--------------------------');
                    console.log('Insert time for ' + 10000 * count + ' values: ' + (end - begin) / 1000);
                    console.log('--------------------------\n');
                    mainloop.emit('ready', 'steps');
                }
                else
                {
                    iterations -= 1;
                    mainloop.emit('populate');
                }
            });
        });

        client.execute(
            token,
            "CREATE TABLE steps (users.id INT, shoes.id INT, FACT step INT)",
            '', '',
            function (err) {
                if (err)
                {
                    console.log(err.why);
                    console.log(err.misc);
                    throw err;
                }
                mainloop.emit('populate');
            }
        );
    }
};
