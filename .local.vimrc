let g:ctrlp_custom_ignore = {
  \ 'dir':  'node_modules$',
  \ 'link': '\v\.thrift$',
  \ }
